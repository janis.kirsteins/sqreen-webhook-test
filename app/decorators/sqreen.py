import hmac
import hashlib

from functools import wraps
from flask import request, abort

def require_valid_webhook_signature(sqreen_secret, logger):
    """Verifies that the request includes the correct X-Sqreen-Integrity header."""

    if sqreen_secret is None: abort(500)
    if isinstance(sqreen_secret, str): sqreen_secret = bytes(sqreen_secret , 'latin-1')

    def actual_decorator(f):
        @wraps(f)
        def func_wrapper(*args, **kwargs):
            sqreen_signature = request.headers.get("X-Sqreen-Integrity")

            if sqreen_signature is None:
                logger.error("The X-Sqreen-Integrity header was not present.")
                abort(400)

            logger.debug("Sqreen signature is present: %s" % sqreen_signature)

            hasher = hmac.new(sqreen_secret, request.get_data(), hashlib.sha256)
            dig = hasher.hexdigest()

            if not hmac.compare_digest(dig, sqreen_signature):
                logger.error("Invalid signature. Expected signature: %s" % dig)
                abort(400)

            return f(*args, **kwargs)
        return func_wrapper
    return actual_decorator
