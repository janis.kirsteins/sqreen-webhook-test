# -*- coding: utf-8 -*-
"""The dispatcher package.

It contains a Dispatcher class:

    from dispatcher import Dispatcher

And it will automatically load the available
backends from a sub-package, and attempt to use them.

All backends will by default be disabled. If a package
finds valid configuration (via environment variables), it will
enable itself.
"""

import os
import pkgutil
from importlib import import_module

from .dispatcher_impl import Dispatcher
from .backends.abstract_base_backend import AbstractBaseBackend

def load_default_backends():
    """Loads all the backends found in the backends sub-package."""
    pkg_path = os.path.dirname(os.path.realpath(__file__))
    backend_path = os.path.join(pkg_path, "backends")

    for (_, name, _) in pkgutil.iter_modules([backend_path]):
        import_module('.backends.' + name, package=__name__)

    # Enable all the known backends by default
    Dispatcher.ENABLED_BACKENDS = AbstractBaseBackend.__subclasses__()

load_default_backends()
