# -*- coding: utf-8 -*-
"""Dispatcher class accepts an object parsed from JSON data,
and dispatches it to different backends."""

import logging
from injector import inject

from .backends.abstract_base_backend import AbstractBaseBackend

class Dispatcher:
    #pylint: disable=too-few-public-methods
    """Dispatcher class implementation - sends."""

    # Initialized upon importing this package
    # (by iterating through the backends subpackage)
    ENABLED_BACKENDS = []

    @inject
    def __init__(self, logger: logging.Logger):
        self.logger = logger

    def process_payload(self, payload):
        """Instantiates a backend for every class in BACKENDS, and
        invokes its process_payload method."""
        self.logger.debug("Sending Sqreen webhook notification to %s" % Dispatcher.ENABLED_BACKENDS)

        for backend_cls in Dispatcher.ENABLED_BACKENDS:
            instance = backend_cls()
            if isinstance(instance, AbstractBaseBackend):
                instance.process_payload(payload, self.logger)
            else:
                self.logger.warn(
                    "Ignoring invalid backend " +
                    "(not a child class of AbstractBaseBackend): %s" % backend_cls)
