import os
import json
import requests

from .abstract_base_backend import AbstractBaseBackend

class SlackBackend(AbstractBaseBackend):
    def process_payload(self, payload, logger):
        slack_url = os.environ.get('BACKEND_SLACK_URL')
        if slack_url is None:
            logger.warn("Please enable the Slack backend by setting the BACKEND_SLACK_URL environment variable.")
            return

        logger.debug("Using slack webhook %s" % slack_url)

        for item in payload:
            message = item["humanized_description"]

            logger.debug("Posting '%s' to slack" % message)
            try:
                r = requests.post(
                    slack_url,
                    data=json.dumps({"text": message}),
                    headers={'Content-Type': 'application/json'}
                )

                print("Req result %s" % r)

                if r.status_code is not 200:
                    logger.error("Posted the message to Slack successfully, but received response code: %d" % r.status_code)
            except requests.exceptions.RequestException as e:
                logger.error("Failed to post the message to Slack: %s" % e)
