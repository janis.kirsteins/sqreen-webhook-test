# -*- coding: utf-8 -*-
"""Contains the AbstractBase for dispatcher backends."""

from abc import ABC, abstractmethod

class AbstractBaseBackend(ABC):
    @abstractmethod
    def process_payload(self, payload, logger):
        pass
