from .abstract_base_backend import AbstractBaseBackend

class LoggerBackend(AbstractBaseBackend):
    def process_payload(self, payload, logger):
        for item in payload:
            logger.info(item["humanized_description"])
