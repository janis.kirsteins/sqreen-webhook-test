# -*- coding: utf-8 -*-
"""Configuration."""

import os

SQREEN_TOKEN = os.environ.get("SQREEN_TOKEN", default=None)
if not SQREEN_TOKEN:
    raise ValueError("Please set the SQREEN_TOKEN environment variable")

SQREEN_WEBHOOK_SECRET = os.environ.get("SQREEN_WEBHOOK_SECRET", default=None)
if not SQREEN_WEBHOOK_SECRET:
    raise ValueError("Please set the SQREEN_WEBHOOK_SECRET environment variable")
